<?php
/**
 * @file
 * Administration section of the majordomo module.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * How many rows of data are shown on our admin pages.
 */
define('MAJORDOMO_ADMIN_ENTRIES_PER_PAGE', 50);

/**
 * Generates the admin page for majordomo.
 *
 * @return string
 */
function majordomo_admin_lists() {
  $output = '';
  $count = db_result(db_query("SELECT COUNT(lid) FROM {majordomo_lists}"));
  $output .= '<p>'. t('You have %cnt mailing lists defined.', array('%cnt' => $count)) .'</p>';
  $output .= '<p>'. l(t('Create a new mailing list'), 'admin/settings/majordomo/lists/add') .'</p>';
  $header = array(
    array('data' => t('List name'), 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Majordomo'), 'field' => 'majordomo'),
    array('data' => t('Public'), 'field' => 'public'),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  $rows = array();
  $query = "SELECT * FROM {majordomo_lists}";
  $query .= tablesort_sql($header);
  $result = pager_query($query, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
  while ($row = db_fetch_array($result)) {
    $rows[] = array(
      array('data' => check_plain($row['name'])),
      array('data' => check_plain($row['majordomo'])),
      array('data' => ((int)$row['public'] === 1) ? t('Yes') : t('No')),
      array('data' => l(t('Edit'), 'admin/settings/majordomo/lists/add/'. (int)$row['lid'])),
      array('data' => l(t('Delete'), 'admin/settings/majordomo/lists/delete/'. (int)$row['lid'])),
      array('data' => l(t('Subscriptions'), 'admin/settings/majordomo/subscriptions/'. (int)$row['lid'])),
    );
  } // while
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
  return $output;
} // function majordomo_admin_lists

/**
 * Generate a form for adding or editing a mailing list.
 *
 * @param array $form_state
 * @param int $lid
 * @return array
 */
function majordomo_admin_add_list($form_state, $lid = 0) {
  $lid = (int)$lid;
  $listinfo = array();
  if ($lid > 0) {
    $listinfo = majordomo_get_list($lid);
  }
  $form = array();

  $form['lid'] = array(
    '#type' => 'value',
    '#value' => !empty($listinfo['lid']) ? $listinfo['lid'] : 0,
  );
  $form['name'] = array(
    '#title' => t('Name'),
    '#description' => t('The name of the mailing list e.g. foo-example-com.'),
    '#type' => 'textfield',
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => (string)$listinfo['name'],
  );
  $form['majordomo'] = array(
    '#title' => t('Majordomo address'),
    '#description' => t('The email address of the majordomo controlling this list e.g. majordomo@example.com.'),
    '#type' => 'textfield',
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => (string)$listinfo['majordomo'],
  );
  $form['public'] = array(
    '#title' => t('Public list'),
    '#description' => t('Enable this checkbox to allow users with the apropriate permissions to subscribe and unsubscribe themselfes to or from the list.'),
    '#type' => 'checkbox',
    '#default_value' => (int)$listinfo['public'],
  );
  $form['password'] = array(
    '#title' => t('List password'),
    '#description' => t('If the list is defined as "closed" majordomo will require an approval password upon each command. You can enter it here. <p><strong>Beware that it will be stored in plain text within the drupal database!</strong></p>'),
    '#type' => 'password',
    '#default_value' => (string)$listinfo['password'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
} // function majordomo_admin_add_list

/**
 * Validation function for majordomo_admin_add_list().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_add_list_validate($form, &$form_state) {
  // Check for a valid email address syntax.
  if (!preg_match('/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $form_state['values']['majordomo'])) {
    form_set_error('majordomo', t('Please enter a valid email address!'));
  }
  if (!empty($form_state['values']['password'])) {
    if (drupal_strlen($form_state['values']['password']) != drupal_strlen(trim($form_state['values']['password']))) {
      form_set_error('password', t('Using whitespaces in your list password will not work with majordomo!'));
    }
  }
} // function majordomo_admin_add_list_validate

/**
 * Submit function for majordomo_admin_add_list().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_add_list_submit($form, &$form_state) {
  majordomo_save_list($form_state['values']['lid'], $form_state['values']['name'], $form_state['values']['majordomo'], $form_state['values']['public'], $form_state['values']['password']);
  $form_state['redirect'] = 'admin/settings/majordomo/lists';
} // function majordomo_admin_add_list_submit

/**
 * Generates a form for deleting a list.
 *
 * @param array $form_state
 * @param int $lid
 * @return void
 */
function majordomo_admin_delete_list($form_state, $lid) {
  $lid = (int)$lid;
  $listinfo = majordomo_get_list($lid);
  $form = array();
  if (!empty($listinfo)) {
    $form['lid'] = array(
      '#type' => 'value',
      '#value' => $lid,
    );
    $form['question'] = array(
      '#prefix' => '<div style="font-weight: bold;">',
      '#value' => t('Are you sure you want to delete the mailing list %l from the database?', array('%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')),
      '#suffix' => '</div>',
    );
    $form['notice'] = array(
      '#prefix' => '<div>',
      '#value' => t('If you delete the list no changes are made to majordomo e.g. all user subscriptions will still exist afterwards.'),
      '#suffix' => '</div>'
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Yes'),
    );
    $form['cancel'] = array(
      '#value' => l(t('No'), 'admin/settings/majordomo/lists'),
    );
  }
  return $form;
} // function majordomo_admin_delete_list

/**
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_delete_list_submit($form, &$form_state) {
  if (!empty($form_state['values']['lid'])) {
    $listinfo = majordomo_get_list($form_state['values']['lid']);
    majordomo_delete_list($form_state['values']['lid']);
    drupal_set_message(t('Mailing list %l was deleted from the database.', array('%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
  }
  $form_state['redirect'] = 'admin/settings/majordomo/lists';
} // function majordomo_admin_delete_list_submit

/**
 * Generates a website for managing list subscriptions.
 *
 * @param int $lid The list.{id} of the mailing list.
 * @return array
 */
function majordomo_admin_subscriptions($lid = 0) {
  $output = '';
  $lid = (int)$lid;
  $listinfo = majordomo_get_list($lid);
  if (!empty($listinfo)) {
    $output = '<h3>'. t('Manage subscriptions for %l.', array('%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')) .'</h3>';
    $count = db_result(db_query("SELECT COUNT(uid) FROM {majordomo_status} WHERE lid = %d", $lid));
    $output .= '<p>'. t('You have %cnt users subscribed to this mailing lists.', array('%cnt' => $count)) .'</p>';
    $header = array(
      array('data' => t('User name'), 'field' => 'b.name', 'sort' => 'asc'),
      array('data' => t('Operations'), 'colspan' => 1),
    );
    $rows = array();
    $query = "SELECT a.lid AS lid, a.uid AS uid, b.name AS name FROM {majordomo_status} AS a JOIN {users} AS b ON a.uid = b.uid WHERE a.lid = ". $lid;
    $query .= tablesort_sql($header);
    $result = pager_query($query, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
    while ($row = db_fetch_array($result)) {
      $rows[] = array(
        array('data' => l($row['name'], drupal_get_path_alias('user/'. $row['uid']))),
        array('data' => l(t('Unsubscribe'), 'admin/settings/majordomo/subscriptions/unsubscribe/'. (int)$row['lid'] .'/'. (int)$row['uid'])),
      );
    } // while
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
  }
  else {
    $output = '<h3>'. t('Please choose a mailing list.') .'</h3>';
    $header = array(
      array('data' => t('List name'), 'field' => 'name', 'sort' => 'asc'),
    );
    $rows = array();
    $query = "SELECT * FROM {majordomo_lists}";
    $query .= tablesort_sql($header);
    $result = pager_query($query, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
    while ($row = db_fetch_array($result)) {
      $users = (int)db_result(db_query("SELECT COUNT(uid) FROM {majordomo_status} WHERE lid = %d", $row['lid']));
      $rows[] = array(
        array('data' => l($row['name'], 'admin/settings/majordomo/subscriptions/'. (int)$row['lid'])),
        array('data' => t('@c subscribed users.', array('@c' => $users))),
      );
    } // while
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
  }
  return $output;
} // function majordomo_admin_subscriptions

/**
 * Generates a form to subscribe an user to a mailing list.
 *
 * @param array $form_state
 * @param int $lid The list.{id} of the mailing list.
 * @param int $uid The user.{id} of the drupal user.
 * @return array
 */
function majordomo_admin_subscribe($form_state, $lid = 0, $uid = 0) {
  $lid = (int)$lid;
  $uid = (int)$uid;
  $form = array();

  $account = user_load($uid);
  $form['name'] = array(
    '#title' => t('Username'),
    '#description' => t('Please enter the name of the user here.'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => $account->name,
    '#required' => TRUE,
  );
  $lists = array();
  $listinfo = majordomo_get_list($lid);
  if (!empty($listinfo)) {
    $lists[$listinfo['lid']] = $listinfo['name'];
  }
  else {
    $result = db_query("SELECT * FROM {majordomo_lists}");
    while ($row = db_fetch_array($result)) {
      $lists[$row['lid']] = $row['name'];
    } // while
  }
  $form['lid'] = array(
    '#title' => t('Mailing list'),
    '#description' => t('Please choose the mailing list the user should be subscribed to.'),
    '#type' => 'select',
    '#options' => $lists,
    '#default_value' => $lid,
    '#required' => TRUE,
  );
  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  $form['cancel'] = array(
    '#value' => l(t('Cancel'), 'admin/settings/majordomo/subscriptions'),
  );

  return $form;
} // function majordomo_admin_subscribe

/**
 * Submit function for majordomo_admin_subscribe().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_subscribe_submit($form, &$form_state) {
  $listinfo = majordomo_get_list($form_state['values']['lid']);
  $account = user_load(array('name' => $form_state['values']['name']));
  if (!empty($listinfo) && !empty($account) && user_access('administer majordomo')) {
    if (!majordomo_is_subscribed($listinfo['lid'], $account->uid)) {
      if (_majordomo_subscribe($listinfo['lid'], $account->mail)) {
        _majordomo_update_user_status($listinfo['lid'], $account->uid, TRUE);
        drupal_set_message(t('User %n is now subscribed to the list %l.', array('%n' => $account->name, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
      }
      else {
        drupal_set_message(t('An error occured while trying to subscribe to the mailing list!'), 'error');
      }
    }
    else {
      drupal_set_message(t('The user %u is already subscribed to the mailing list %l!', array('%n' => $account->name, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')), 'error');
    }
  }
  $form_state['redirect'] = 'admin/settings/majordomo/subscriptions';
} // function majordomo_admin_subscribe_submit

/**
 * Generates a form to unsubscribe an user to a mailing list.
 *
 * @param array $form_state
 * @param int $lid The list.{id} of the mailing list.
 * @param int $uid The user.{id} of the drupal user.
 * @return array
 */
function majordomo_admin_unsubscribe($form_state, $lid = 0, $uid = 0) {
  $lid = (int)$lid;
  $uid = (int)$uid;
  $form = array();

  $account = user_load($uid);
  $form['name'] = array(
    '#title' => t('Username'),
    '#description' => t('Please enter the name of the user here.'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => $account->name,
    '#required' => TRUE,
  );
  $lists = array();
  $listinfo = majordomo_get_list($lid);
  if (!empty($listinfo)) {
    $lists[$listinfo['lid']] = $listinfo['name'];
  }
  else {
    $result = db_query("SELECT * FROM {majordomo_lists}");
    while ($row = db_fetch_array($result)) {
      $lists[$row['lid']] = $row['name'];
    } // while
  }
  $form['lid'] = array(
    '#title' => t('Mailing list'),
    '#description' => t('Please choose the mailing list the user should be unsubscribed from.'),
    '#type' => 'select',
    '#options' => $lists,
    '#default_value' => $lid,
    '#required' => TRUE,
  );
  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe'),
  );
  $form['cancel'] = array(
    '#value' => l(t('Cancel'), 'admin/settings/majordomo/subscriptions'),
  );

  return $form;
} // function majordomo_admin_unsubscribe

/**
 * Submit function for majordomo_admin_unsubscribe().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_unsubscribe_submit($form, &$form_state) {
  $listinfo = majordomo_get_list($form_state['values']['lid']);
  $account = user_load(array('name' => $form_state['values']['name']));
  if (!empty($listinfo) && !empty($account) && user_access('administer majordomo')) {
    if (_majordomo_unsubscribe($listinfo['lid'], $account->mail)) {
      _majordomo_update_user_status($listinfo['lid'], $account->uid, FALSE);
      drupal_set_message(t('User %n was unsubscribed from the list %l.', array('%n' => $account->name, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
    }
    else {
      drupal_set_message(t('An error occured while trying to unsubscribe from the mailing list!'), 'error');
    }
  }
  $form_state['redirect'] = 'admin/settings/majordomo/subscriptions';
} // function majordomo_admin_unsubscribe_submit

/**
 * Generates a form to subscribe a role to a mailing list.
 *
 * @param array $form_state
 * @param int $lid The list.{id} of the mailing list.
 * @param int $rid The role.{id} of the role.
 * @return array
 */
function majordomo_admin_subscribe_role($form_state, $lid = 0, $rid = 0) {
  $lid = (int)$lid;
  $rid = (int)$rid;
  $form = array();

  $role = NULL;
  if ($rid > 0) {
    $role = (string)db_result(db_query("SELECT name FROM {role} WHERE rid = %d", $rid));
  }
  $form['name'] = array(
    '#title' => t('Role name'),
    '#description' => t('Please enter the name of the role here.'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'roles/autocomplete',
    '#default_value' => $role,
    '#required' => TRUE,
  );
  $lists = array();
  $listinfo = majordomo_get_list($lid);
  if (!empty($listinfo)) {
    $lists[$listinfo['lid']] = $listinfo['name'];
  }
  else {
    $result = db_query("SELECT * FROM {majordomo_lists}");
    while ($row = db_fetch_array($result)) {
      $lists[$row['lid']] = $row['name'];
    } // while
  }
  $form['lid'] = array(
    '#title' => t('Mailing list'),
    '#description' => t('Please choose the mailing list all users of the selected role should be subscribed to.'),
    '#type' => 'select',
    '#options' => $lists,
    '#default_value' => $lid,
    '#required' => TRUE,
  );
  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  $form['cancel'] = array(
    '#value' => l(t('Cancel'), 'admin/settings/majordomo/subscriptions'),
  );
  $form['notice'] = array(
    '#prefix' => '<div style="font-weight: bold;">',
    '#value' => t('If a role contains many users this process can take quite some time e.g. several minutes!'),
    '#suffix' => '</div>',
  );

  return $form;
} // function majordomo_admin_subscribe_role

/**
 * Validation function for majordomo_admin_subscribe_role().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_subscribe_role_validate($form, &$form_state) {
  $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $form_state['values']['name']);
  $role = db_fetch_array($result);
  if ($role['rid'] == DRUPAL_ANONYMOUS_RID) {
    form_set_error('name', t('You cannot subscribe anonymous users to a mailing list!'));
  }
} // function majordomo_admin_subscribe_role_validate

/**
 * Submit function for majordomo_admin_subscribe_role().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_subscribe_role_submit($form, &$form_state) {
  $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $form_state['values']['name']);
  $role = db_fetch_array($result);
  $listinfo = majordomo_get_list($form_state['values']['lid']);
  if (!empty($role) && !empty($listinfo) && user_access('administer majordomo')) {
    if ($role['rid'] == DRUPAL_AUTHENTICATED_RID) {
      $result = db_query("SELECT uid FROM {users} WHERE status = 1");
    }
    else {
      $result = db_query("SELECT uid FROM {users_roles} WHERE rid = %d", $role['rid']);
    }
    $cnt = 0;
    $err = 0;
    $skp = 0;
    while ($row = db_fetch_array($result)) {
      $account = user_load($row['uid']);
      if (!majordomo_is_subscribed($listinfo['lid'], $account->uid)) {
        if (_majordomo_subscribe($listinfo['lid'], $account->mail)) {
          _majordomo_update_user_status($listinfo['lid'], $account->uid, TRUE);
          $cnt++;
        }
        else {
          $err++;
        }
      }
      else {
        $skp++;
      }
    } // while
    drupal_set_message(t('@c users were subscribed to the list %l.', array('@c' => $cnt, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
    if ($skp > 0) {
      drupal_set_message(t('@s users were skipped because the were already subscribed to the list.', array('@s' => $skp)), 'warning');
    }
    if ($err > 0) {
      drupal_set_message(t('@e errors occured while subscribing users to the mailing list!', array('@e' => $err)), 'error');
    }
  }
  $form_state['redirect'] = 'admin/settings/majordomo/subscriptions';
} // function majordomo_admin_subscribe_role_submit

/**
 * Generates a form to unsubscribe a role to a mailing list.
 *
 * @param array $form_state
 * @param int $lid The list.{id} of the mailing list.
 * @param int $rid The role.{id} of the role.
 * @return array
 */
function majordomo_admin_unsubscribe_role($form_state, $lid = 0, $rid = 0) {
  $lid = (int)$lid;
  $rid = (int)$rid;
  $form = array();

  $role = NULL;
  if ($rid > 0) {
    $role = (string)db_result(db_query("SELECT name FROM {role} WHERE rid = %d", $rid));
  }
  $form['name'] = array(
    '#title' => t('Role name'),
    '#description' => t('Please enter the name of the role here.'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'roles/autocomplete',
    '#default_value' => $role,
    '#required' => TRUE,
  );
  $lists = array();
  $listinfo = majordomo_get_list($lid);
  if (!empty($listinfo)) {
    $lists[$listinfo['lid']] = $listinfo['name'];
  }
  else {
    $result = db_query("SELECT * FROM {majordomo_lists}");
    while ($row = db_fetch_array($result)) {
      $lists[$row['lid']] = $row['name'];
    } // while
  }
  $form['lid'] = array(
    '#title' => t('Mailing list'),
    '#description' => t('Please choose the mailing list all users of the selected role should be unsubscribed from.'),
    '#type' => 'select',
    '#options' => $lists,
    '#default_value' => $lid,
    '#required' => TRUE,
  );
  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe'),
  );
  $form['cancel'] = array(
    '#value' => l(t('Cancel'), 'admin/settings/majordomo/subscriptions'),
  );
  $form['notice'] = array(
    '#prefix' => '<div style="font-weight: bold;">',
    '#value' => t('If a role contains many users this process can take quite some time e.g. several minutes!'),
    '#suffix' => '</div>',
  );

  return $form;
} // function majordomo_admin_unsubscribe_role

/**
 * Validation function for majordomo_admin_unsubscribe_role().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_unsubscribe_role_validate($form, &$form_state) {
  $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $form_state['values']['name']);
  $role = db_fetch_array($result);
  if ($role['rid'] == DRUPAL_ANONYMOUS_RID) {
    form_set_error('name', t('You cannot unsubscribe anonymous users from a mailing list!'));
  }
} // function majordomo_admin_unsubscribe_role_validate

/**
 * Submit function for majordomo_admin_unsubscribe_role().
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function majordomo_admin_unsubscribe_role_submit($form, &$form_state) {
  $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $form_state['values']['name']);
  $role = db_fetch_array($result);
  $listinfo = majordomo_get_list($form_state['values']['lid']);
  if (!empty($role) && !empty($listinfo) && user_access('administer majordomo')) {
    if ($role['rid'] == DRUPAL_AUTHENTICATED_RID) {
      $result = db_query("SELECT uid FROM {users} WHERE status = 1");
    }
    else {
      $result = db_query("SELECT uid FROM {users_roles} WHERE rid = %d", $role['rid']);
    }
    $cnt = 0;
    $err = 0;
    while ($row = db_fetch_array($result)) {
      $account = user_load($row['uid']);
      if (_majordomo_unsubscribe($listinfo['lid'], $account->mail)) {
        _majordomo_update_user_status($listinfo['lid'], $account->uid, FALSE);
        $cnt++;
      }
      else {
        $err++;
      }
    } // while
    drupal_set_message(t('@c users were unsubscribed from the list %l.', array('@c' => $cnt, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
    if ($err > 0) {
      drupal_set_message(t('@e errors occured while unsubscribing users from the mailing list!', array('@e' => $err)), 'error');
    }
  }
  $form_state['redirect'] = 'admin/settings/majordomo/subscriptions';
} // function majordomo_admin_unsubscribe_role_submit

/**
 * An autocomplete function for role names.
 */
function majordomo_role_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_query_range("SELECT name FROM {role} WHERE LOWER(name) LIKE LOWER('%s%%')", $string, 0, 10);
    while ($role = db_fetch_object($result)) {
      $matches[$role->name] = check_plain($role->name);
    }
  }

  drupal_json($matches);
}

/**
 * Generates the subscriptions manage page for the users account.
 *
 * @param stdClass $account The user information object.
 * @return string
 */
function majordomo_user_subscriptions($account) {
  $output = '';
  global $user;
  if ($account->uid == $user->uid || user_access('administer majordomo')) {
    $count = (int)db_result(db_query("SELECT COUNT(lid) FROM {majordomo_lists} WHERE public = 1"));
    if ($count > 0) {
      $header = array(
        array('data' => t('List name'), 'field' => 'name', 'sort' => 'asc'),
        array('data' => t('Operations'), 'colspan' => 1),
      );
      $query = "SELECT * FROM {majordomo_lists} WHERE public = 1";
      $query .= tablesort_sql($header);
      $result = pager_query($query, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
      $rows = array();
      while ($row = db_fetch_array($result)) {
        if (majordomo_is_subscribed($row['lid'], $account->uid)) {
          $rows[] = array(
            array('data' => check_plain($row['name'] .' ('. $row['majordomo'] .')')),
            array('data' => l(t('Unsubscribe'), 'user/'. $account->uid .'/majordomo/unsubscribe/'. $row['lid'])),
          );
        }
        else {
          $rows[] = array(
            array('data' => check_plain($row['name'] .' ('. $row['majordomo'] .')')),
            array('data' => l(t('Subscribe'), 'user/'. $account->uid .'/majordomo/subscribe/'. $row['lid'])),
          );
        }
      } // while
      $output .= theme('table', $header, $rows);
      $output .= theme('pager', NULL, MAJORDOMO_ADMIN_ENTRIES_PER_PAGE);
    }
    else {
      $output .= '<p>'. t('No public mailing lists are available.') .'</p>';
    }
  }
  else {
    drupal_set_message(t('You are not authorized to access this page!'));
  }
  return $output;
} // function majordomo_user_subscriptions

/**
 * Subscribes a user to a mailing list.
 *
 * @param stdClass $account The user information object.
 * @param int $lid The list.{id} of the mailing list.
 * @return void
 */
function majordomo_user_subscribe($account, $lid) {
  $lid = (int)$lid;
  $listinfo = majordomo_get_list($lid);
  if ((user_access('access public majordomo lists') && !empty($listinfo) && (int)$listinfo['public'] === 1) || user_access('administer majordomo')) {
    if (_majordomo_subscribe($lid, $account->mail)) {
      _majordomo_update_user_status($lid, $account->uid, TRUE);
      drupal_set_message(t('You (%n) are now subscribed to the list %l.', array('%n' => $account->name, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
    }
    else {
      drupal_set_message(t('An error occured while trying to subscribe to the mailing list!'), 'error');
    }
  }
  drupal_goto('user/'. $account->uid .'/majordomo');
} // function majordomo_user_subscribe

/**
 * Unsubscribes a user from a mailing list.
 *
 * @param stdClass $account The user information object.
 * @param int $lid The list.{id} of the mailing list.
 * @return void
 */
function majordomo_user_unsubscribe($account, $lid) {
  $lid = (int)$lid;
  $listinfo = majordomo_get_list($lid);
  if ((user_access('access public majordomo lists') && !empty($listinfo) && (int)$listinfo['public'] === 1) || user_access('administer majordomo')) {
    if (_majordomo_unsubscribe($lid, $account->mail)) {
      _majordomo_update_user_status($lid, $account->uid, FALSE);
      drupal_set_message(t('You (%n) are now unsubscribed from the list %l.', array('%n' => $account->name, '%l' => $listinfo['name'] .' ('. $listinfo['majordomo'] .')')));
    }
    else {
      drupal_set_message(t('An error occured while trying to unsubscribe from the mailing list!'), 'error');
    }
  }
  drupal_goto('user/'. $account->uid .'/majordomo');
} // function majordomo_user_unsubscribe